#include <vector>
#include <iterator>
#include <iostream>
#include <sstream>
#include <string>

using std::string;

//split an std::string with delim, return individual string vector
std::vector<string>& split(const string& s, char delim,
                           std::vector<string>& elems){
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss,item, delim)){
        elems.push_back(item);
    }
    return elems;
}

//transform an std::string to vector<string> elements.
std::vector<string>& stringToVectorString(const string& s){
    std::vector<string> sv;
    split(s," ", sv);
    std::string<string> rv;
    reverseVector(sv, rv);
    std::copy(rv.begin(), rv.end(),
              std::ostream_iterator<std::vector<string>>(std::cout," ") );
}

string reverseWord(string& word){
    
    return "";
}

//generic vector reverser
template<typename T>
void reverseVector(const std::vector<T>& orig, std::vector<T>& mod){
    //create the iterator of vector<T>
    typename std::vector<T>::const_iterator it;
    for(it = orig.end()-1; it >= orig.begin(); --it){
        mod.push_back(*it);
    }
}


void reverseIt(){
    std::vector<int> v1;
    std::vector<int> v2;
    
    for(int i = 1; i <= 10; ++i){
        v1.push_back(i);
    }
    
    reverseVector(v1, v2);
    
    std::ostream_iterator<int> out(std::cout,", ");
    std::copy(v2.begin(), v2.end(), out);
    
}
int main(int argc, char** argv){
    
    reverseIt();
    return 0;
}