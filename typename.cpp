#include <iostream>

template <typename T> struct Base {
    void f(){
        std::cout << "Base<T>::f()\n";
    }
};

template <typename T> struct Derived: Base<T> {
    void g(){
        std::cout<< "Derived<T>::g\n";
        this->f();
    }
};


struct Foo{
    template<typename U>
    static void foo(){
        std::cout<<"template<U> foo()\n";
    }
};

template<typename T> void func(T* p){
    //Error, name lookup fails
    //T::foo<T>();
    
    //explicitly state foo is a template function
    T::template foo<T>();
}
int main(){
    Foo f;
    func(&f);
    return 0;
}