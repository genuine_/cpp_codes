#include <vector>
#include <iterator>
#include <iostream>
#include <sstream>
#include <string>

int fib(int first, int second, int sum, int max)
{
    if (first+second+sum > max)
    return sum;
    
    if ((first+second) %2 != 0)
    {
        return fib(second, (first+second), sum, max);
    }
    sum += first+second;
    std::cout << first<< "   " << second << "   " << sum << "     " << std::endl;
    return fib(second, (first+second), sum,max);
}

int main(int argc, char** argv)
{
    std::cout << fib(0,1,0,4000000) << std::endl;
}

