#include <iostream>
#include <ostream>
#include <iterator>
#include <algorithm>
#include <vector>

//using std::vector;

//vector<long> factors;

long pfactor( long num){
    int s = 2;
    while(s * s <= num){
        if(num % s == 0){
            num /= s;
        }
        else{
            s++;
        }
    }
    return num;
}

int main(int argc, char** argv){
    long r = pfactor(600851475143);
    std::cout << "Largest prime: " << r <<"\n";
    return 0;
}