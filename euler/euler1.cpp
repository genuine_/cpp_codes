#include <iostream>

int main(int argc, char** argv){
    int sum = 0;
    
    for (int x=3; x < 1000; x++) {
        if(x%3==0 || x%5==0){
            sum+=x;
        }
    }
    std::cout<<"sum is: " << sum << std::endl;
    return 0;
}